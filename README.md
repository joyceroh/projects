# TiltedMaze Application
TiltedMaze is a game application in which the user tries to control a ball through a maze (from one end to the other end) by tilting the device to indicate the direction that the ball should move.
There are two types of walls— one where the user “dies” if the ball touches the wall and another type that is just a standard wall.
The user has a minute to get through the maze; otherwise, the user loses.
The user loses in one of two ways: the user touches the “death” wall or time runs out before the user gets to the other side.
Otherwise, the user wins and their best time is saved locally. 
Upon game completion, the user is directed to a scoreboard screen that displays the top scores and is able to add his or her own name to the scoreboard if the score is high enough.

Watch sample video here: https://youtu.be/dtkPtQZORTc

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing
1. Configure each Mac and iOS device you plan to test with an iCloud account. Create or use an existing Apple ID account that supports iCloud.
2. Configure the Team for each target within the project.
3. Open the project in the Project navigator within Xcode and select each of the targets. Set the Team on the General tab to the team associated with your developer account.
4. Change the Bundle Identifier.
5. With the project's General tab still open, update the Bundle Identifier value. You should modify the reverse DNS portion to match the format com.yourdomain.Lister

## Built With
XCode 11.2.1

## Authors
Joyce Roh, Minki Kim, Julia Dai, and Jessica Wang