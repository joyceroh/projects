//
//  GameOverViewController.swift
//  TiltedMaze
//
//  Created by Julia Dai on 11/26/19.
//  Copyright © 2019 Julia Dai. All rights reserved.
//

import UIKit

class GameOverViewController: UIViewController {
    @IBOutlet weak var winLabel: UILabel!
    @IBOutlet weak var finishTime: UILabel!
    @IBOutlet weak var saveScoreButton: UIButton!
    var timeFinished: Int?
    //var wonGame: Bool?
    var hitPink: Bool?
    var hardMode: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(timeFinished! != 60 && !hitPink!) {
            winLabel.text = "You Won!"
            finishTime.text = "You finished in: " + String(timeFinished!) + " seconds"
            saveScoreButton.isHidden = false
        }
        else {
            winLabel.text = "Game Over"
            finishTime.text = "You lost."
            saveScoreButton.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    @IBAction func playAgain(_ sender: Any) {
        if hardMode! {
            performSegue(withIdentifier: "PlayAgainHard", sender: self)
        }
        else {
            performSegue(withIdentifier: "PlayAgain", sender: self)
        }
        
    }
    @IBAction func backToMenu(_ sender: Any) {
        
        performSegue(withIdentifier: "BackToMenu", sender: self)
    }
    @IBAction func saveScore(_ sender: Any) {
        let alert = UIAlertController(title: "Save High Score", message: "Please enter your name", preferredStyle: .alert)
        let action = UIAlertAction(title: "Save", style: .default) { (alertAction) in
            let textField = alert.textFields![0] as UITextField
            let score: Double = Double(self.timeFinished!)
            let name: String = textField.text!
            
            let defaults = UserDefaults.standard
            var highScores = defaults.array(forKey: "SavedIntArray")  as? [Int] ?? [Int]()
            var highScoresNames = defaults.stringArray(forKey: "SavedStringArray") ?? [String]()
            
            var added: Bool = false
            for i in 0...4 {
                 if i < highScores.count && !added  {
                    if Int(score) < highScores[i] {
                        highScores.insert(Int(score), at: i)
                         highScoresNames.insert(name, at: i)
                         if highScores.count > 5 {
                             highScores.removeLast()
                             highScoresNames.removeLast()
                         }
                         added = true
                     }
                 }
             }
             
            if highScores.count < 5 && !added {
                highScores.append(Int(score))
                 highScoresNames.append(name)
             }
            
            defaults.set(highScoresNames, forKey: "SavedStringArray")
            defaults.set(highScores, forKey: "SavedIntArray")
            
        }
        alert.addTextField { (textField) in
            textField.placeholder = "Enter your name"
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
