//
//  GameScene.swift
//  TiltedMaze
//
//  Created by Joyce Roh on 11/15/19.
//  Copyright © 2019 Joyce Roh. All rights reserved.
//

import SpriteKit
import GameplayKit
import CoreMotion

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let manager = CMMotionManager()
    let ball = SKSpriteNode(imageNamed: "Ball")
    var gameOver = false
    var hitPink = false
    var viewController: GameViewController?
    var startTime = 60
    var timer = Timer()
    var finishTime = 0
    
    lazy var timerLabel: SKLabelNode = {
        let label = SKLabelNode()
        label.text = "60"
        label.fontSize = 72
        label.fontColor = .black
        label.position = CGPoint(x: 310, y: 780)
        return label
    }()

    override func didMove(to view: SKView) {
        addChild(timerLabel)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(GameScene.game), userInfo: nil, repeats: true)
        
        self.ball.size = CGSize(width: 100, height: 100)
        self.childNode(withName: "Ball")?.addChild(self.ball)
        
        manager.startAccelerometerUpdates()
        manager.accelerometerUpdateInterval = 0.1
        
        let ACHandler : CMAccelerometerHandler = {
            data, error in
            self.physicsWorld.gravity = CGVector(dx: CGFloat((data?.acceleration.x)!) * 10, dy: CGFloat((data?.acceleration.y)!) * 10)
        }
        
        manager.startAccelerometerUpdates(to: OperationQueue.main, withHandler: ACHandler)
        self.physicsWorld.contactDelegate = self
    }
    func didBegin(_ contact: SKPhysicsContact) {
        let player = contact.bodyA
        let pinkWall = contact.bodyB
        
        if player.categoryBitMask == 1 && pinkWall.categoryBitMask == 2 || player.categoryBitMask == 2 && pinkWall.categoryBitMask == 1 {
            print("death occured")
            hitPink = true
            gameOver = true
            self.endGame()
        }
        
    }
    func endGame() {
        print("end game")
        self.childNode(withName: "Ball")?.removeAllChildren()
        /*if let scene = GameOverScene(fileNamed: "GameOverScene") {
            scene.viewController = self.viewController
            if let view = scene.viewController?.view as! SKView? {
                view.presentScene(scene)
            }
        }*/
        self.viewController?.time = finishTime
        self.viewController?.hitPink = hitPink
        self.viewController?.performSegue(withIdentifier: "GameOver", sender: self)
    }
    
    override func update(_ currentTime: TimeInterval) {

        if(!gameOver && convertPoint(toView: self.childNode(withName: "Ball")!.position).y < 100) {
            gameOver = true
            self.endGame()
        }
    }
    
    @objc func game() {
        startTime = startTime - 1
        finishTime = finishTime + 1
        timerLabel.text = String(startTime)
        
        if (startTime == 0) {
            timer.invalidate()
            //include a pop-up that says game over
            self.endGame()
        }
    }
    
}
