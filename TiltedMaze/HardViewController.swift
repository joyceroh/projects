//
//  HardViewController.swift
//  TiltedMaze
//
//  Created by Joyce Roh on 12/1/19.
//  Copyright © 2019 Joyce Roh. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class HardViewController: UIViewController {

    var time: Int?
       var hitPink: Bool?
       
       override func viewDidLoad() {
           super.viewDidLoad()
           
           if let view = self.view as! SKView? {
               // Load the SKScene from 'GameScene.sks'
               if let scene = HardGameScene(fileNamed: "HardGameScene") {
                   scene.viewController = self
                   // Set the scale mode to scale to fit the window
                   scene.scaleMode = .aspectFill
                   
                   // Present the scene
                   view.presentScene(scene)
               }
               
               view.ignoresSiblingOrder = true
               
               view.showsFPS = true
               view.showsNodeCount = true
           }
       }
       
       override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           if let vc = segue.destination as? GameOverViewController {
               vc.timeFinished = time
               vc.hitPink = hitPink
            vc.hardMode = true
           }
       }
       
       override var shouldAutorotate: Bool {
           return true
       }

       override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
           if UIDevice.current.userInterfaceIdiom == .phone {
               return .allButUpsideDown
           } else {
               return .all
           }
       }

       override var prefersStatusBarHidden: Bool {
           return true
       }

}
