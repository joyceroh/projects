//
//  MenuViewController.swift
//  TiltedMaze
//
//  Created by Joyce Roh on 11/15/19.
//  Copyright © 2019 Joyce Roh. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func goScores(_ sender: Any) {
        performSegue(withIdentifier: "HighScores", sender: self)
    }
    
    @IBAction func playGame(_ sender: Any) {
        performSegue(withIdentifier: "PlayGame", sender: self)
        
    }
    
    @IBAction func playHard(_ sender: Any) {
        performSegue(withIdentifier: "HardGame", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
