//
//  ScoreViewController.swift
//  TiltedMaze
//
//  Created by Joyce Roh on 11/15/19.
//  Copyright © 2019 Joyce Roh. All rights reserved.
//

import UIKit

class ScoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {


    @IBOutlet weak var scoreTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scoreTable.delegate = self
        self.scoreTable.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func goHome(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let defaults = UserDefaults.standard
        let highScores = defaults.array(forKey: "SavedIntArray")  as? [Int] ?? [Int]()
        let highScoresNames = defaults.stringArray(forKey: "SavedStringArray") ?? [String]()
        
        let cell = UITableViewCell()
        if(indexPath.row < highScores.count) {
            cell.textLabel?.text = highScoresNames[indexPath.row] + ": " + String(highScores[indexPath.row]) + " seconds"
            print(indexPath.row)
        }
        
        return cell
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
